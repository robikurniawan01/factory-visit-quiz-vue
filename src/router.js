import { createRouter, createWebHashHistory } from 'vue-router';
import Home from './views/Home.vue';
import Agreement from './views/AgreementView.vue';
import Join from './views/JoinView.vue';
import Sign from './views/SignInView.vue';
import Lobby from './views/LobbyView.vue';
import Quiz from './views/QuizView.vue';

const routes = [
  {
    path: '/',
    redirect: '/join',
  },
  {
    path: '/home',
    name: 'Home',
    component: Home,
  },
  {
    path: '/agreement',
    name: 'agreement',
    component: Agreement,
  },
  {
    path: '/join',
    name: 'join',
    component: Join,
  },
  {
    path: '/sign',
    name: 'sign',
    component: Sign,
  },
  {
    path: '/lobby',
    name: 'lobby',
    component: Lobby,
  },
  {
    path: '/quiz',
    name: 'quiz',
    component: Quiz,
  },
];

const router = createRouter({
  history: createWebHashHistory('/otsukaquizz/'),
  routes,
});

export default router;