import { createApp } from 'vue'
// import './style.css'
import App from './App.vue'
import router from './router';

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap/dist/js/bootstrap.min'
import { LottieAnimation } from "lottie-web-vue";
import Vue3Storage from "vue3-storage";
import Loading from 'vue-loading-overlay';
import 'vue-loading-overlay/dist/css/index.css';

const app = createApp(App);

app.use(router);
app.use(Vue3Storage);
app.component('lottie-animation', LottieAnimation);
app.component('VueLoading', Loading);

app.mount('#app');